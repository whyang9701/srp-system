from app import app
from flask import render_template,send_from_directory,jsonify
import mysql.connector


@app.route('/api/get_member')
def getMember():
    mydb = mysql.connector.connect(host="127.0.0.1",user="root",passwd="phillips9701",database="SRP")
    mycursor = mydb.cursor()

    mycursor.execute("SELECT * FROM member")

    myresult = mycursor.fetchall()
    return jsonify(myresult)

@app.route('/api/get_team')
def getTeam():
    mydb = mysql.connector.connect(host="127.0.0.1",user="root",passwd="phillips9701",database="SRP")
    mycursor = mydb.cursor()

    mycursor.execute("SELECT * FROM team")

    myresult = mycursor.fetchall()
    return jsonify(myresult)

@app.route('/api/get_memberGroupByTeam')
def getMemberGroupByTeam():
    mydb = mysql.connector.connect(host="127.0.0.1",user="root",passwd="phillips9701",database="SRP")
    mycursor = mydb.cursor()
    mycursor.execute("SET sql_mode=(SELECT REPLACE(@@sql_mode,'ONLY_FULL_GROUP_BY',''));")
    mycursor.execute("SELECT teamID , id FROM member group by teamID")

    myresult = mycursor.fetchall()
    return jsonify(myresult)


@app.route('/')
def index():
    return render_template('index.html')

@app.route('/<path:path>')
def sendPage(path):
    return render_template((path+'.html'))


@app.route('/images/<path:path>')
def send_img(path):
    return send_from_directory('templates/images', path)

@app.route('/css/<path:path>')
def send_css(path):
    return send_from_directory('templates/css', path)


